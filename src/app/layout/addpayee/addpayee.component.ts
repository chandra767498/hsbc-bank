import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-addpayee',
  templateUrl: './addpayee.component.html',
  styleUrls: ['./addpayee.component.scss']
})
export class AddpayeeComponent implements OnInit {
  Normalform = new FormGroup({});
  sender: any = {};
  adminData: any;
  paramsObj: any = {};
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
    public router: Router
  ) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
  }

  ngOnInit(): void {
  }
  saveData() {
    try {
      this.sender['payeer_account_number'] = this.adminData.account_number
      this.appService.postMethod('addpayee', this.sender).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.sender = {};
            swal.fire(resp.msg, 'success');
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
}





