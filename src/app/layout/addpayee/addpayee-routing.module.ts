import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddpayeeComponent } from './addpayee.component';


const routes: Routes = [
  {
    path: '',
    component : AddpayeeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddpayeeRoutingModule { }
