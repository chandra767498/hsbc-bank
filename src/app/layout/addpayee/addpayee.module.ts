import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddpayeeRoutingModule } from './addpayee-routing.module';
import { AddpayeeComponent } from './addpayee.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [AddpayeeComponent],
  imports: [
    CommonModule,
    AddpayeeRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    TranslateModule,
    MatCardModule,
    MatIconModule

  ]
})
export class AddpayeeModule { }
