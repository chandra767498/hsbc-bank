import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtherpayeeRoutingModule } from './otherpayee-routing.module';
import { OtherpayeeComponent } from './otherpayee.component';


@NgModule({
  declarations: [OtherpayeeComponent],
  imports: [
    CommonModule,
    OtherpayeeRoutingModule
  ]
})
export class OtherpayeeModule { }
