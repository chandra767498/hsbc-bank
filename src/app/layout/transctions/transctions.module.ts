import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCard, MatCardModule } from '@angular/material/card';
import { TransctionsRoutingModule } from './transctions-routing.module';
import { TransctionsComponent } from './transctions.component';


@NgModule({
  declarations: [TransctionsComponent],
  imports: [
    CommonModule,
    TransctionsRoutingModule,
    MatCardModule
  ]
})
export class TransctionsModule { }
