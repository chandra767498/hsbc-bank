import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-transctions',
  templateUrl: './transctions.component.html',
  styleUrls: ['./transctions.component.scss']
})
export class TransctionsComponent implements OnInit {
  items: any;
  adminData:any={};
  constructor( 
    private appService: AppService,
    private cookieService: CookieService,
    public router: Router) 
    { 
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

  ngOnInit(): void {
    this.getTransaction();
  }
  // getAllTransaction() {
  //   try {
  //     this.appService.getMethod('getAllTransaction').subscribe(
  //       (resp: any) => {
  //         if (resp.success) {
  //           this.items = resp.data;
  //           console.log(resp);
  //         } else {
  //           swal.fire(resp.msg, 'error');
  //         }
  //       },
  //       (error) => { }
  //     );
  //   } catch (e) { }
  // }
  getTransaction() {
    try {
      const data = {
        payeer_account_number: this.adminData.account_number
      };
      this.appService.postMethod('getTransaction', data ).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.items = resp.data;
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  }


