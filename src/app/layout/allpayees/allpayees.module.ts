import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllpayeesRoutingModule } from './allpayees-routing.module';
import { AllpayeesComponent } from './allpayees.component';


@NgModule({
  declarations: [AllpayeesComponent],
  imports: [
    CommonModule,
    AllpayeesRoutingModule
  ]
})
export class AllpayeesModule { }
