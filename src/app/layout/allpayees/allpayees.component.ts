import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-allpayees',
  templateUrl: './allpayees.component.html',
  styleUrls: ['./allpayees.component.scss']
})
export class AllpayeesComponent implements OnInit {
items: any;
adminData:any={};
  constructor(
    private appService: AppService,
    private cookieService: CookieService,
    public router: Router
  ) { 
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
  }

  ngOnInit(): void {
  //  this. getAllpayeers();
   this.getPayeersData()
  }
  getAllpayeers() {
    try {
      this.appService.getMethod('getAllpayeers').subscribe(
        (resp: any) => {
          if (resp.success) {
            this.items = resp.data;
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  getPayeersData() {
    try {
      const data = {
        payeer_account_number: this.adminData.account_number
      };
      this.appService.postMethod('getPayeers',data ).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.items = resp.data;
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  payee(event, data) {
    this.router.navigate(['/sendmoney'], data);
}
}


