import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllpayeesComponent } from './allpayees.component';


const routes: Routes = [
  {
    path: '',
    component: AllpayeesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllpayeesRoutingModule { }
