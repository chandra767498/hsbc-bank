import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginData: any = {};
  loginError: string;
  LoginForm: FormGroup;
  isLogin = false;
  adminData: any;
  ImageData = [];
ImageArray = [];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private appService: AppService,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
  }

  submitForm() {
    try {
      this.appService.postMethod('auth', this.loginData).subscribe((resp: any) => {
          if (resp.success) {
            localStorage.setItem('isLoggedin', 'true');
            this.cookieService.set('adminData', JSON.stringify(resp.data), 1, '/');
            this.router.navigate(['/dashboard']);
          } else {
            swal.fire(resp.msg, 'Something went wrong!', 'error');
          }
        },
          error => {
          });
    } catch (e) { }
  }
  


}
